<?php

namespace Drupal\ucam_jobs;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Return info about jobs from the ucam jobs feed.
 */
class UcamJobsCrawler implements UcamJobsCrawlerInterface {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly ClientInterface $httpClient,
    private readonly LoggerInterface $loggerFactory,
    ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getJobsList(mixed $unitIDs): array {
    $jobIDs = [];

    $url = 'https://www.jobs.cam.ac.uk/job';

    foreach ((array) $unitIDs as $unitID) {
      try {
        $response = $this->httpClient->request('GET', $url, [
          'query' => [
            'unit' => $unitID,
            'format' => 'json',
          ],
        ]);

        $job_ids = [];
        $jobs = (json_decode($response->getBody(), TRUE));

        foreach ($jobs['list']['jobs'] as $job) {
          $jobIDs[] = $job['id'];
        }
      }
      catch (GuzzleException $e) {
        $this->loggerFactory
          ->error('Error retrieving jobs for unit @id: @msg', [
            '@id' => $unitID,
            '@msg' => $e->getMessage(),
          ]);
      }

    }

    return $jobIDs;

  }

  /**
   * {@inheritdoc}
   */
  public function getJobInfo(int $jobID): array {
    $url = "https://www.jobs.cam.ac.uk/job/$jobID";

    $info = [];

    try {
      $response = $this->httpClient->request('GET', $url, [
        'query' => ['format' => 'json'],
      ]);

      $info = json_decode($response->getBody(), TRUE);
    }
    catch (GuzzleException $e) {
      $this->loggerFactory
        ->error('Error retrieving jobs for job @id: @msg', [
          '@id' => $jobID,
          '@msg' => $e->getMessage(),
        ]);
      return [];
    }

    return $info['job'];
  }

}
