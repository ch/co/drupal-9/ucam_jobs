<?php

namespace Drupal\ucam_jobs;

/**
 * Interface for job updater service.
 */
interface UcamJobsUpdaterInterface {

  /**
   * Create nodes for all jobs in the feed.
   */
  public function importNodes();

  /**
   * Update all job nodes.
   */
  public function updateAllNodes();

  /**
   * Update one job node.
   *
   * @param int $nid
   *   The node ID to update.
   */
  public function updateNode(int $nid);

  /**
   * Delete all job nodes with a past closing date.
   */
  public function deleteExpiredNodes();

}
