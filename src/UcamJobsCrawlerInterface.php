<?php

namespace Drupal\ucam_jobs;

/**
 * Provides ucam_jobs.jobs_crawler service.
 */
interface UcamJobsCrawlerInterface {

  /**
   * Get List of Jobs.
   *
   * @param mixed $unitID
   *   Institutiion ID (either a string or array of strings)
   *
   * @return array
   *   List of job IDs
   */
  public function getJobsList(mixed $unitID) : array;

  /**
   * Get info about a job.
   *
   * @param int $jobID
   *   ID of the job.
   *
   * @return array
   *   Info about the job
   */
  public function getJobInfo(int $jobID): array;

}
