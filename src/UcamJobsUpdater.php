<?php

namespace Drupal\ucam_jobs;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\node\Entity\Node;
use Psr\Log\LoggerInterface;

/**
 * Implement ucam_jobs.node_updater service.
 */
class UcamJobsUpdater implements UcamJobsUpdaterInterface {

  /**
   * Constructor.
   */
  public function __construct(
      private readonly UcamJobsCrawlerInterface $jobCrawlerFactory,
      private readonly LoggerInterface $loggerFactory,
      private readonly ConfigFactoryInterface $configFactory,
      ) {
  }

  /**
   * {@inheritdoc}
   */
  public function importNodes() {
    $config = $this->configFactory->get('ucam_jobs.settings');
    $institionIds = $config->get('institution_ids');

    $extantIds = $this->getExtantJobIds();

    foreach ($institionIds as $inst) {
      $jobIds = $this->jobCrawlerFactory->getJobsList($inst);
      foreach ($jobIds as $jobId) {

        if (in_array($jobId, $extantIds)) {
          continue;
        }

        $node = Node::create(['type' => 'job']);
        $node->set('title', $jobId);
        $node->set('field_job_id', $jobId);
        $node->save();
        $this->updateNode($node->id());
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function updateAllNodes() {
    $nids = $this->getAllJobNodeIds();
    foreach ($nids as $nid) {
      $this->updateNode($nid);
    }
  }

  /**
   * Get the node ids of all job nodes.
   *
   * @return array
   *   array of nids of all ucam_job nodes
   */
  private function getAllJobNodeIds(): array {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'job')
      ->accessCheck(FALSE);
    $nids = $query->execute();

    return $nids;
  }

  /**
   * Get job IDs of all our job nodes.
   *
   * @return array
   *   Array of IDs.
   */
  private function getExtantJobIds() {
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    $extantNids = $this->getAllJobNodeIds();
    $nodes = $node_storage->loadMultiple($extantNids);

    $extantIds = [];

    foreach ($nodes as $node) {
      $extantIds[] = $node->get('field_job_id')->value;
    }

    return $extantIds;

  }

  /**
   * {@inheritdoc}
   */
  public function deleteExpiredNodes() {

    $query = \Drupal::entityQuery('node')
      ->condition('type', 'job')
      ->condition('field_job_closing_date', date("Y-m-d"), "<")
      ->accessCheck(FALSE);

    $nids = $query->execute();

    foreach ($nids as $nid) {
      $node = Node::Load($nid);
      $node->delete();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function updateNode(int $nid) {
    $node = Node::Load($nid);
    $jobID = $node->get('field_job_id')->value;

    $jobInfo = $this->jobCrawlerFactory->getJobInfo($jobID);

    // getJobInfo() returns [] if the response is 404. This happens
    // if a job has been deleted from upstream - we should remove it
    // too.
    if (empty($jobInfo)) {
      $node->delete();
      return;
    }

    $node->set('title', $jobInfo['title']);
    $node->set('body', ['value' => $jobInfo['advert']['html'], 'format' => 'full_html']);
    if (array_key_exists('salary', $jobInfo)) {
      $salary = $jobInfo['salary']['display'];
      // The json-formatted feed tends to have \r\n after "or"
      // when more than one salary range is specified.
      $salary = str_replace(['\r', '\n'], ' ', $salary);
      $salary = preg_replace('|\s+|', ' ', $salary);
      $node->set('field_job_salary', $salary);
    }
    $node->set('field_job_reference', $jobInfo['reference']);
    $node->set('field_job_closing_date', $jobInfo['dates']['closes']);
    $node->set('field_job_link', $jobInfo['url']);

    $tid = $this->getTidForCategory($jobInfo['category']['name']);
    if ($tid) {
      $node->set('field_job_category', $tid);
    }

    if (array_key_exists('attachments', $jobInfo)) {
      $node->set('field_job_further_info', [
        'uri' => $jobInfo['attachments'][0]['url'],
        'title' => $jobInfo['attachments'][0]['reference'],
      ]);
    }

    $node->save();

  }

  /**
   * Get taxonomy term ID for a category.
   *
   * @param string $category
   *   The job category to look up.
   *
   * @return mixed
   *   The term id for the category, if found.
   */
  private function getTidForCategory(string $category): ?int {
    $vid = 'ucam_job_categories';
    $term = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties([
        'name' => $category,
        'vid' => $vid,
      ]);

    if (!empty($term)) {
      return array_shift($term)->id();
    }
  }

}
