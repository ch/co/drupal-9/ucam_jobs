<?php

namespace Drupal\ucam_jobs\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Ucam Job info block.
 *
 * @Block(
 *  id = "ucam_job",
 *  admin_label = "Ucam Job Info",
 *  context_definitions = {
 *    "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *  }
 * )
 */
class UcamJobsInfo extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->getContextValue('node');

    $closingDate = $node->get('field_job_closing_date')->date;
    $formattedClosingDate = \Drupal::service('date.formatter')->format($closingDate->getTimestamp(), 'ucam_jobs');
    $jobref = $node->get('field_job_reference')->value;
    $salary = $node->get('field_job_salary')->value;
    $furtherInfo = $node->get('field_job_further_info');

    $content = [
      '#type' => 'container',
      '#attributes' => [],
      '#cache' => [
        'tags' => [
          'node:' . $node->id(),
        ],
      ],
    ];

    $content['closingdate'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'campl-content-container'],
      'header' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => 'Closing date',
        '#attributes' => ['class' => 'campl-heading-container'],
      ],
      'contents' => ['#markup' => $formattedClosingDate],
    ];

    if (!empty($jobref)) {
      $content['jobref'] = [
        '#type' => 'container',
        '#attributes' => ['class' => 'campl-content-container'],
        'heading' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => 'Reference number',
          '#attributes' => ['class' => 'campl-heading-container'],
        ],
        'contents' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $jobref,
        ],
      ];
    }

    if (!empty($salary)) {
      $content['salary'] = [
        '#type' => 'container',
        '#attributes' => ['class' => 'campl-content-container'],
        'heading' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => 'Salary',
          '#attributes' => ['class' => 'campl-heading-container'],
        ],
        'contents' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $salary,
        ],
      ];
    }

    if (!empty($furtherInfo->uri)) {
      if (!empty($furtherInfo->title)) {
        $infoTitle = $furtherInfo->title;
      }
      else {
        $infoTitle = 'Further information';
      }
      $infoUri = $furtherInfo->uri;
      $content['furtherinfo'] = [
        '#type' => 'container',
        '#attributes' => ['class' => 'campl-content-container'],
        'heading' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => 'Further Information',
          '#attributes' => ['class' => 'campl-heading-container'],
        ],
        'contents' => [
          '#type' => 'link',
          '#title' => $infoTitle,
          '#url' => Url::fromUri($infoUri),
        ],
      ];
    }

    return $content;
  }

}
