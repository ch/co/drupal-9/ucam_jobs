<?php

namespace Drupal\ucam_jobs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config form for job importer.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ucam_jobs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ucam_jobs_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ucam_jobs.settings');
    $values = $config->get('institution_ids') ?: [];

    $form['institution_ids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List of Institution IDs to import'),
      '#description' => 'Enter the www.jobs.cam.ac.uk <em>Unit ID</em> for which jobs should be imported.<br/>If you want to import jobs for more than one institution, enter a list of IDs separated by spaces, e.g.<br/><ul><li>u00083</li><li>u00225 u00083</li></ul><strong>Note:</strong> to determine an institution\'s ID,<ol><li>Find a current job advertised by the instiution on <a href="http://www.jobs.cam.ac.uk">http://www.jobs.cam.ac.uk</a></li><li>Click on the institution\'s name under "DEPARTMENT/LOCATION"</li><li>Note the webpage address in the browser address bar; it will be of the form <em>http://www.jobs.cam.ac.uk/job/?unit=u00225</em></li><li>The institution\'s ID is the text after <em>unit=</em> consisting of five digits preceeded by the letter "u" (so u00225 in this example)</ol>',
      '#default_value' => implode(" ", $values),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $idString = trim($form_state->getValue('institution_ids'));

    if (!preg_match('/^(?:(?:u[[:digit:]]{5}) *)*$/', $idString)) {
      $form_state->setErrorByName('institution_ids', 'Error setting Institution IDS: An institution\'s ID must be the letter "u" followed by 5 digits. If more than one institution ID is specified, they must be separated by spaces.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ucam_jobs.settings');
    $idString = trim($form_state->getValue('institution_ids'));
    $config->set('institution_ids', explode(" ", $idString));
    $config->save();
    parent::submitForm($form, $form_state);

    /** @var Drupal\ucam_jobs\UcamJobsUpdater $nodeUpdaterService */
    $nodeUpdaterService = \Drupal::service('ucam_jobs.node_updater');
    $nodeUpdaterService->importNodes();

  }

}
